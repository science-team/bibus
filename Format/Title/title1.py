# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
from Utilities.title_case import title_case

def format(s,uselocale,f=0):
	"""Reformat Title.
	if f=0 => This is the title
	if f=1 => This Is The Title
	if f=2 => THIS IS THE TITLE
	if f=3 => this is the title"""
	s = s.strip()
	if f == 0:
		return s.capitalize()
	elif f == 1:
		return title_case(s)
	elif f == 2:
		return s.upper()
	elif f == 3:
		return s.lower()
	else:
		return s
