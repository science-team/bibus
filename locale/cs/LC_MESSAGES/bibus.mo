��    �     �    ,      �!  F  �!     �"     #     #      #     )#     +#     =#  -   M#     {#     ~#     �#     �#     �#     �#     �#     �#     �#     �#     �#  
   �#  0   $  	   3$     =$     C$  	   G$     Q$  	   `$     j$     x$     �$  	   �$     �$     �$     �$     �$  
   �$     �$     �$     �$     %     %     2%  #   A%  w   e%  M  �%     +'     1'     6'  	   ;'     E'     N'  7   U'     �'     �'     �'     �'     �'     �'  /   �'     (      (     3(     H(     `(  !   |(  &   �(     �(     �(     �(     �(     �(     �(     
)     )     ")     ')     -)     D)  !   Z)  !   |)     �)     �)     �)     �)     �)     �)     �)     �)     �)     *     *     +*     =*     N*     \*     u*     |*     �*     �*     �*  $   �*  
   �*     �*     +     +     +     1+     ?+     D+  
   I+     T+     Y+     b+  
   r+     }+     �+     �+     �+     �+     �+     �+     �+     �+     �+      ,     ,     ',  "   <,     _,     {,     �,     �,  
   �,     �,     �,     �,     �,     �,     �,     �,     �,     -     -     5-     U-     m-     z-  	   �-     �-     �-     �-     �-     �-     �-     �-  
   .     .  '   5.     ].     r.     ~.     �.     �.  *   �.     �.     �.     �.  *   �.     /     3/     M/     T/     c/     x/     �/     �/  )   �/     �/     �/     �/     0     0  '   20  �   Z0     1      1     (1     11     :1     F1     R1  
   Z1  H   e1  $   �1     �1     �1     �1     2     2     -2  )   B2     l2     t2     �2     �2     �2     �2     �2     �2     �2     �2     �2     �2     �2  	   3  	   3     3     #3  	   *3     43     93     J3     Q3     V3     ^3     m3  	   3     �3     �3     �3     �3     �3     �3     �3     4  
   4     4     4     (4     14     74     =4  	   B4     L4  %   l4     �4     �4     �4      �4      5  $   5     B5     G5     P5     n5     z5     �5  	   �5     �5  	   �5     �5     �5     �5     �5     �5     
6     6  
   6     "6     &6     *6     06  	   @6     J6     [6     m6     ~6     �6  
   �6     �6     �6     �6     �6     �6     �6     �6     7     7     7  
   )7     47     ;7  
   K7     V7     d7  	   }7     �7  
   �7  
   �7  	   �7     �7     �7     �7     �7  
   �7      8     8     '8     <8     M8  	   Z8     d8  
   m8     x8  0   8  Z   �8  >   9  M   J9  =   �9  g   �9  "   >:  <   a:     �:     �:     �:  
   �:      �:     �:     ;     ;     ;  	   %;     /;     <;     P;     c;  
   {;  	   �;     �;     �;     �;     �;     �;  
   �;     �;     �;     <     <     &<     /<  ;   ;<  5   w<  '   �<  [   �<  p   1=  #   �=  	   �=     �=  	   �=     �=     �=  %   �=      >     3>     K>     j>     �>     �>     �>     �>     �>     �>     �>     �>     ?     ?  �   ?  K   �?  B   @  #   T@  3   x@  c   �@     A     A     A     %A  $   -A     RA     XA     gA     oA     |A     �A     �A     �A     �A     �A     �A     �A  {  	B  .  �C     �D     �D     �D     �D     �D     �D     	E  0    E     QE     TE     WE     ZE     cE     ~E  	   �E     �E     �E  "   �E     �E     �E  /   �E     F     -F     0F     9F     JF  	   ]F     gF     uF     �F  
   �F     �F     �F     �F     �F     �F     �F     G     G     #G     7G     KG  ,   eG  m   �G  @   H     AI     II     PI     XI     eI     nI  6   uI     �I     �I     �I     �I     �I     �I  (   J     8J     HJ     ^J     vJ     �J  )   �J     �J     �J  	   �J     K     
K     K     %K     :K     GK  
   MK     XK     _K     ~K  (   �K  $   �K     �K     �K     L     L  	   L     'L     6L     KL     ]L     nL     �L     �L     �L     �L     �L     �L     �L     �L     
M     $M  &   9M  	   `M     jM     xM     �M     �M     �M     �M     �M  
   �M     �M     �M     �M     �M     N     N     'N     BN     IN     UN  !   [N     }N     �N  
   �N     �N     �N     �N  )   �N      O     ?O     LO  
   YO     dO     pO     �O     �O     �O     �O  
   �O     �O     �O     �O     �O     P  "   )P     LP     ZP     gP     uP     }P     �P     �P     �P     �P     �P     Q      Q  +   @Q     lQ     �Q     �Q  
   �Q     �Q  #   �Q     �Q     �Q     �Q  -   R     ;R     YR  
   uR     �R     �R     �R     �R     �R  .   �R     &S     8S     SS     [S     jS  '   �S  �   �S     ^T     gT     pT     |T     �T     �T     �T     �T  S   �T  ,   U     ;U     GU     cU     pU     �U     �U  (   �U     �U     �U     V     V     /V     DV     KV     SV     eV     lV     |V     �V     �V     �V     �V     �V     �V     �V     �V     �V  
   �V  	   W     W     W     -W     >W     LW     OW     VW  %   dW     �W  !   �W     �W     �W     �W     �W     �W     X     	X     X     X     X  %   (X     NX     mX     �X     �X     �X     �X     �X     Y     Y     Y     0Y     6Y     >Y     JY     SY     aY     jY     }Y     �Y     �Y     �Y     �Y     �Y     �Y     �Y  	   �Y     �Y     �Y  	   Z     Z     "Z     8Z     HZ     ZZ  	   hZ     rZ     �Z     �Z     �Z     �Z     �Z     �Z     �Z     
[     [     #[     4[     =[     R[     _[      r[     �[     �[     �[     �[     �[     �[     �[     �[     \     \     \     .\     N\  !   g\     �\     �\     �\     �\     �\  0   �\  f   ]  G   h]  K   �]  M   �]  g   J^  6   �^  M   �^     7_     K_     ^_     x_  #   �_     �_     �_     �_     �_     �_     �_     �_     `     `     )`     6`     C`     P`     a`     q`     x`  	   �`     �`     �`     �`  
   �`     �`     �`  5   �`  <   .a  !   ka  b   �a  h   �a  *   Yb     �b     �b     �b     �b  !   �b  %   �b     �b     
c  &    c  )   Gc     qc     �c     �c     �c     �c     �c     �c     �c     d     d  �   d  V   �d  P   +e  #   |e  ]   �e  ^   �e     ]f     af  
   gf     rf  (   zf     �f  #   �f     �f  
   �f     �f     �f     
g  	   $g  !   .g     Pg     Xg     hg     �       8      @   m      Z   5  �           {   ^   G      ?       |      D   �         �   �   q   �   U  �   .  `           �       ~       �       �              �   o      j   r        �   �   l   �  c   D      "       �   z   ]  �   �          K          i       /   >      w   �   �   E      q  �           �         f  7      6   �   ,       n  �       �     9  -  �   6  P      �   �     3      &      �                   �          �           �              �   g  �           ;   4       B  �           �   �   �   x          9           �       v              �   &   X   �       B       )   �                  Q   �         F   �  L   M   /  �       �   �             �         #  �   u   o        E  �  �   I      �  �   Y  �   U       �   �   �               )  �   M      �     �      �      W   e   �   �   	  �   k  �   �   �   ]   g           u      Z  �   1   .       �   �   l  
  �                     #      8           f           �                 +            �   �    ^      d  �     N   '           �   �       J       =     ;  �   <      R       
       �       [   �   N  �       �   k   �  %  C       h   V      A  A           �   +       V       2   s   $      _  (              }   x  t   S   	   �   F  �   [  ?      �   P      c  �       �   :       '     �   �  �   i      �   �   �   �   �  �   �   �       �   m      ~      7       �   y   X  Q      z  �             `  |      *  p  n       �   0  y  1  b  2    �   Y       �   �       �   �   >   �   !   C       a   @  �             �  J      R      �           �      �       �   O     \  4      H   �       K   {  �   e  (   �              S     �   �  d   �   s  G   *   v   �  �   $       I   0       �   �   r          w      �     _   �  <       h  ,  �   W  �   �     H       \   �   j     O   -   �     5   !  �       b   �       %   p   �       t  �   3    �          :  �   �       T   �   T              �   �     �   �   "  �   L  =  �           a  }   
Bibus can directly insert citations in OpenOffice.org
if you start OpenOffice.org in 'listening mode'.


Do you want to activate this mode by default?


A document will open in OpenOffice.org,
Click on 'Accept UNO connections',
Then restart OpenOffice.org.
Under Windows, you must also quit the OpenOffice.org Quickstarter.

 %s connection parameters %s references : %s selected ,  , et al. - - Click on Cancel - Click on Next - Follow the instructions in mysql_config.txt -- :  ;  ARTICLE Abbreviate author list with Acrobat file Activate Add Add Text Add a child to the selected key Add a field Add a line Add a reference associated with the selected key Add child After All All files All references Anonymous Anonymous ... Anonymous author format As in database Ascending Author Author list Author list format Author-Date Base Style Before last BibTeX Bibliographic index BibliographicType Bibliography title Bibus WEB site Bibus may use two database engines: Bibus needs a USERNAME to identify yourself.
Please enter a name in the box below.
Your login name might be a good idea Bibus uses a file to store the bibliographic database.
Please enter in the box below the path to 
the bibliographic database you want to use.
- If the file EXIST, Bibus will 
	- not modify it
	- assume it to be a correct Bibus database.
- If the file DOES NOT EXIST, Bibus will 
	- create it
	- use it as your bibliographic database. Black Blue Bold Booktitle Brackets Cancel Cannot connect to Openoffice. See Documentation.
%s
%s) Caps Capture ... Capture Citations Capture from database Capture from field Capture selection Capture the references absent from the database Case Change author list Check for duplicates Choose a File to import Choose the destination file Choose the file location and name Choose the name of the SQLite database Citation Cited Clear Connect Connect to database Connection settings Connection type Content Copy Count Create Index on Insert Create a new category Create the Bibliography if absent Creating or choosing the database Creating styles ... Current key Cut Cyan Database Database = %s Database = None Database Error Database Name Database choice Database engine Database file ... Database file... Database type Database used at startup Delete Delete query Delete reference Delete the selected reference Delete this key Deleting old index and citations ... Descending Directory ... Display Displayed fields Document position Documentation Done Down Duplicates Edit Edit ... Edit Identifier Edit query Edit reference Edit shortcuts ... Edit the selected reference Editor Encoding Error Error during query Expert Expert Search Export Export a file Export to Medline format Export to RIS format Export to Refer format for EndNote Export to a SQLite database Field Field Value Field color Field name Field name color Fields Fields formatting Fields ordering File File ... File name ... Fill character in tab stops Finalize Finalize the document Finalizing the current document First Connection Wizard First Editor First author First key Format Format Author as Format Bibliography Format Editor as Format fields as Format reference as Formating citations ... Formatting Fuse: [1] [3] [2] -> [1; 3; 2] Fuse: [Martin] [John] -> [Martin; John] Fusing citations ... GoTo Writer Green Help Hilight Citations Hilight citations with a yellow background Host If a duplicate is found If at least If author Field is empty, replace it with: If you want to use MySQL If you want to use SQLite Import Import && Quit Import a BibTeX file Import a Medline file Import a RIS file Import a Refer file Import a Refer file exported from EndNote Import a file Import a text file Insert Insert Citation Inserting citations in text Inserting citations in text (%s ranges) It seems that your database is not compatible with your PySQLite version.
If it does not work,
see the SQLite site about database format change in SQLite3 http://www.sqlite.org/version3.html Italic Joining Key type Language Last Editor Last author License Light_grey List all authors on first occurence if authors number is not higher than List more authors until it is unique Load ... Looking for duplicates ... Main Fields Main Fields = %s Main fields (+ Abstract) Main fields searched Making a copy of the current document ... Medline Medline Search Microsoft Word Document Middle Editors Middle authors Mode Month MySQL or SQLite Name Name format New New ... New Name format New Query New query New reference NewKey No Choice None Norma Jean Baker Normal Note Nothing Number entries Number of records Numbering OK Online Open URL OpenOffice connection settings OpenOffice.org OpenOffice.org Text Document OpenOffice.org connection Other Fields Page Setup Pages PassWord Password Paste Paths Pipe Pipe name Please choose the file encoding Please enter password for database %s Please select a style file Please select the file Please, enter the new name Please, enter the new query name Please, enter the query name Please, select the default directory Port Position Position of the tab stop (mm) Preferences Preview Preview ... Preview : Preview printing Print ... Print references Printed fields Printing Pubmed search ... Put a letter after the year Queries Quit Quit Bibus RIS Red Refer Refer (EndNote) Reference Reference Editor Reference display Reference fields Reference list Reference type References References formatting Remove Rename Query Rename folder Rename query Rename this key SQLite SQlite database file Save Save as ... Save as... Search Search && Close Search ... Search PubMed Search Pubmed on the Web Search in Search the database Second key Select All Selection Separate authors with Separator 1 Separator 2 Separator 3 Separators Separators ... Set printer page format Set some preferences Setting username Settings ... Shortcuts Show All Small Caps Socket Solving duplicates ... (%s series of duplicates) Some needed fields are absent from the selected database/tables.
Continue at your own risk Sorry but codec '%s' is not available.
I will try to use ascii Sorry but the SQLite python extension is not available.
Read installation.txt Sorry but the key you are moving already exist in this folder Sorry, but I was not able to find the python module for the %s database.
Please check your installation Sorry, you cannot modify this key. Sorry, you cannot move a reference associated with this key. Sort Ascending Sort Descending Sort bibliography by Sort order Sort: [1] [3] [2] -> [1] [2] [3] Sorting Standard Start at record Style Style ... Style author Style creation date Style informations Style modification date Style name Subscript Superscript Supplementary Fields Supplementary fields TCP/IP Tab stop is right aligned Tabulation Tag Tag reference Tag the selected reference Tagged Template Text Window The identifier was not unique and it has been changed to %s The key must be unique.
Please choose a new key name. The selected style is not a bibus style The version of the style file is too old.
 I will convert it to the new Bibus style format. The version of the style file is too recent. Please update bibus to a new version.
 I will open a default style. There is %s such records in Medline Third key Title Underline Up Update Index on Insert Update the Bibliography automatically Updating index ... Updating references ... Use current language for month Use range: [1] [3] [2] -> [1-3] UserName Username Using separator Warning Welcome to Bibus! Welcome to bibus What do you want to export? Where to save the style file? White Year You can set here the environment variable $FILES.
This variable is substituted when an URL is opened.
This allows you to set a central repository
for all the articles.
 You cannot edit default styles
.I have made a copy of the style for editing You cannot use %r and %r characters.
Please choose a new key name. You did not select a valid database You must first edit Pref/Shortcuts to use this menu You must first save the current document before using this function.
 Should I save it and proceed? ]-[ a Field a String authors authors, replace authors 2 to n with below case sensitive default in 'ARTICLE' in database keep the new reference keep the old reference last save password (Not secure!) shortcut using separator when there are more than Project-Id-Version: novy_bibus
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2005-11-16 13:53+0100
Last-Translator: Dan Rajdl <rajdl@fnplzen.cz>
Language-Team: Czech <cs@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.10
Plural-Forms:  nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
 
Bibus může vkládat citace rovnou do OpenOffice.org
pokud OpenOffice.org naslouchá.
Chcete, abych aktivoval tuto funkci?


Otevře se dokument OpenOffice.org,
klikněte na 'Accept UNO connections',
poté restartujte OpenOffice.org.
Ve Windows je také nutné zavřít OpenOffice.org Quickstarter.

 Parametry spojení %s %s referencí: %s vybráno ,  , et al. - - Klikněte na Storno - Klikněte na Další - Následujte pokynů v souboru mysql_config.txt -- :  ;  Článek Zkrátit seznam autorů na Soubor Akrobatu Aktivovat Přidat Přidat text Přidat potomka vybranému klíči Přidat pole Přidat řádku Přidat referenci spojenou s vybraným klíčem Přidat potomka Po Všechno Všechny soubory Všechny reference Anonymně Anonymní ... Formát autora (anonymní) Jako v databázi Vzestupně Autor Seznam autorů Formát seznamu autorů Autor-Datum Základní styl Před posledním BibTeX Bibliografický index Bibliografický typ Název bibliografie Domovská stránka Bibusu Bibus umí používat 2 databázové stroje: Bibus potřebuje UŽIVATELSKÉ JMÉNO k Vaší identifikaci.
Vložte Vaše Uživatelské jméno do políčka. Bibus používá soubor pro ukládání bibliografické databáze.
Vložte cestu k databázi, kterou hodláte používat.
- Pokud soubor EXISTUJE, Bibus ho
	-nebude modifikovat
	- pokud je to správná databáze Bibusu.
- NEEXISTUJE-LI tento soubor, Bibus ho
	- vytvoří
	-používejte ho jako bibliografickou databázi Černá Modrá Tučně Název knihy Závorky Storno Nemůžu se spojit s Openoffice. Viz Dokumentace.
(%s) VŠECHNA VELKÁ Zachytit ... Zachytit citace Zachytit z databáze Zachytit z pole Zachytit výběr Zachytit citace chybějící v databázi VELKÁ PÍSMENA Změň seznam autorů Zkontrolovat duplikáty Vyberte soubor k importu Vyberte soubor určení Vyberte umístění souboru a jeho jméno Vyberte název SQLite databáze Citace Citované Vymazat Spojit Spojit s databází Spojovací řetězce Typ spojení Obsah Kopírovat Počet Vytvořit index při vložení Vytvořit novou kategorii Vytvořit bibliografii, pokud neexistuje Vytvoření nebo vybrání databáze Vytvářím styly ... Aktuální klíč Vyjmout Modrá Databáze Databáze = %s Databáze = žádná Chyba v databázi Název databáze Výběr databáze Databázový stroj Soubor databáze ... Databázový soubor ... Typ databáze Implicitní databáze Smazat Zrušit dotaz Smazat referenci Smazat vybranou referenci Vymazat tento klíč Mažu starý index a staré citace ... Sestupně Adresář ... Zobrazit Zobrazená pole Místo v dokumentu Dokumentace Hotovo Dolů Duplikáty Upravit Upravit ... Upravit identifikátor Upravit dotaz Upravit referenci Upravit zkratky ... Upravit vybranou referenci Editor Kódování Chyba Chyba během vykonávání dotazu Expert Pokročilé vyhledávání Exportovat Exportovat soubor Exportovat do formátu Medline Exportovat do RIS formátu Exportovat do formátu Refer nebo EndNote Exportovat do databáze SQLite Oblast, pole Hodnota pole Barva pole Název pole Barva názvu pole Pole Formátování polí Řazení polí Soubor Soubor ... Název souboru ... Vyplňte znak v tabulátorech Finalizovat Finalizovat dokument Finalizuji tento dokument Průvodce připojením k databázi První Editor První autor První klíč Formát Formátovat autora jako Naformátovat bibliografii Formátovat Editora jako Formátovat pole jako Naformátuj referenci jako Formátuji citace ... Formátování Spojit: [1] [3] [2] -> [1; 3; 2] Sloučit: [Martin] [John] -> [Martin; John] Slučuji citace ... Přepnout do Office Zelená Nápověda Zvýraznit citace Zvýraznit citace žlutým pozadím Hostitel Je-li nalezen duplikát Pokud alespoň Pokud je pole Autorů prázdné, nadraď jej: Pokud chcete používat MySQL Chcete-li používat SQLite Importovat Importovat && Opustit Importovat soubor BibTeX Importovat soubor Medline Importovat soubor RIS Importovat soubor Refer Importovat soubor Refer exportovaný z EndNotu Importovat soubor Importovat textový soubor Vložit Vložit citaci Vkládám citace do textu Vkládám citace do textu (%s rozsahů) Zdá se, že Vaše databáze je nekompatibilní s Vaší verzí PySQLite.
Pokud to nebude fungovat,
konzultujte domovskou stránku SQLite3 ne http://www.sqlite.org/version3.html Kurzíva Spojení Typ klíče Jazyk Poslední Editor Poslední autor Licence Světle_šedá Vypiš všechny autory při prvním výskytu, pokud je počet autorů nižší než Vypiš více autorů, dokud jsou jedineční Nahrát ... Vyhledávám duplikáty ... Hlavní pole Hlavní pole = %s Hlavní pole (+Abstrakt) Hlavní prohledávaná pole Dělám kopii aktuálního dokumentu ... Medline Vyhledávání v Medline Dokument Microsoft Word Prostřední Editoři Prostřední autoři Režim Měsíc MySQL nebo SQLite Název Název formátu Nový Nový .. Nový formát jména Nový dotaz Nový dotaz Nová reference Nový klíč Bez výběru Žádný Norma Jean Baker Normální Poznámky Ničím Číslovat záznamy Počet záznamů Číslování OK Online Otevřít URL Nastavení připojení OpenOffice.org OpenOffice.org Textový dokument OpenOffice.org. Spojení s OpenOffice.org Další pole Nastavení stránky Strany Heslo Heslo Vložit Cesty Pipe Jméno Pipe Vyberte kódování souboru, prosím. Vložte heslo pro databázi %s Vyberte soubor stylu. Vyberte soubor. Vložte nové jméno, prosím. Vložte nový název dotazu Vložte název dotazu. Vyberte základní adresář Port Umístění Umístění tabulátoru (mm) Volby Náhled Náhled ... Náhled: Náhled tisku Tisk ... Tisknout reference Tištěná pole Tisk Vyhledávání v PubMedu ... Vlož číslici za rok Dotazy Konec Zavřít Bibus RIS Červená Refer Refer (EndNote) Reference Editor referencí Zobrazení referencí Pole referencí Seznam referencí Typ reference Reference Formátování referencí Odebrat Přejmenovat dotaz Přejmenovat adresář Přejmenovat dotaz Přejmenovat tento klíč SQLite Soubor databáze SQLite Uložit Uložit jako ... Uložit jako ... Vyhledat Vyhledat && Zavřít Vyhledat ... Vyhledat v PubMedu Vyhledávání v PubMedu na webu Hledat v Vyhledat v databázi Druhý klíč Vybrat vše Výběr Oddělit autory s  Oddělovač 1 Oddělovač 2 Oddělovač 4 Oddělovače Oddělovače ... Nastavit formát tisku stránky Nastavit některé volby Nastavení uživatelského jména Nastavení ... Klávesové zkratky Ukázat vše Malé kapitálky Soket Řeším duplikáty ... (%s sérií duplikátů) Některá povinná pole ve vybrané databázi/tabulce chybí.
Pokračujte jen na vlastní nebezpečí. Omlouvám se, ale kodek %s není k dispozici.
Zkusím asci kódování. Extenze pythonu pro SQLite není dostupná.
Přečtěte si installation.txt Je mi líto, ale klíč, který přesouváte již v tomto adresáři existuje Omlouvám se, ale nenašel jsem modul pythonu pro databázi %s.
Zkontrolujte, prosím, Vaši instalaci. Omlouvám se, ale nemůžete modifikovat tento klíč. Omlouvám se, ale nemůžete přesunout referenci spojenou s tímto klíčem. Seřadit vzestupně Seřadit sestupně Seřadit bibliografii dle Směr řazení Seřadit [1] [3] [2] -> [1] [2] [3] Řazení Standard Začít na záznamu Styl Styl ... Autor stylu Datum vytvoření stylu Údaje o stylu Datum modifikace stylu Jméno stylu Dolní index Horní index Doplňková pole Vedlejší pole TCP/IP Tabulátor je zarovnán doprava Odsazení Označit Označit referenci Označit vybranou referenci Označené Vzor Textové okno Identifikátor nebyl jedinečný a byl změněn na %s Klíč musí být jedinečný.
Vyberte nové jméno klíče. Vybraný stal není stylem Bibusu Verze stylového souboru je příliš stará.
Převedu ji do nového formátovacího stylu Bibusu. Verze stylového souboru je příliš nová. Aktualizujte Bibus na novou verzi.
Otevřu základní styl. V Medline existuje %s takových záznamů. Třetí kýč Název Podtržení Nahoru Aktualizovat index při vložení Aktualizovat Bibliografii automaticky Aktualizuji index ... Aktualizuji reference Použij aktuální jazyk pro měsíce. Používat rozmezí: [1] [3] [2] -> [1-3] Uživatelské jméno Uživatelské jméno S oddělovačem Upozornění Vítejte v Bibusu! Vítejte v bibusu Co chcete exportovat? Kam mám uložit soubor stylu? Bílá Rok Zde můžete určit proměnnou $FILES.
Tato proměnná je automaticky použita při otevírání URL.
To například umožňuje mít centrální úložiště 
pro všechny články.
 Nemůžete upravovat výchozí styly.
Vytvořil jsem pro Vás kopii stylu k úpravám. Znaky %r a %r nelze v názvu použít.
Vyberte si nový název klíče, prosím. Nevybral(a) jste platnou databázi. Nejdříve musíte upravit záložku Preference/Zkratky, abyste
mohl(a) používat toto menu Před použitím této funkce musíte nejprve dokument uložit.
Mám ho uložit a pokračovat? ]-[ polem řetězcem autorů autoři, nahradit autory 2 na n šířka níže rozlišovat velká a malá písmena implicitní v ČLÁNKU v databázi ponechat novou referenci ponechat starou referenci poslední uložit heslo (není bezpečné!) zkratka s oddělovačem pokud je více než 