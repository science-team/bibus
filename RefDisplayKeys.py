# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
# -*- coding: ISO-8859-1 -*-
# select bibrefLink.key_id from bibrefLink left join bibrefKey on bibrefLink.key_id = bibrefKey.key_id where ref_id=1 and user='pmartino'
# select t2.key_id from bibrefLink as t1,bibrefKey as t2 where t1.key_id = t2.key_id and t1.ref_id=1 and t2.user='pmartino';

import wx
import StringIO,cPickle
import Export.html, BIB

# we use as Base the wx.html.HtmlWindow
from wx.html import HtmlWindow as RefDisplayBase
import wx.html

class RefDisplay(RefDisplayBase):
	def __init__(self, *args, **kwds):
		RefDisplayBase.__init__(self, *args, **kwds)
		# we need a file to use the filter. We use a StringIO
		self.output = StringIO.StringIO()
		self.bibframe = None	# we need the main bibus bibframe for callback and self.db
		self.ref_id = None		# id of currently displayed reference

	def resetFormat(self,format):
		return

	def Clear(self):
		self.output.truncate(0)
		self.SetPage('')

	def display(self,ref):
		"""Display the keys associated with reference ref
		in the keytree of the current db user
		If ref = '' => clear the display
		"""
		self.Clear()
		# we must test if it makes sense
		# ie, if we are not in online or import
		selectedId = self.bibframe.keytree.GetSelection()
		if selectedId.IsOk():
			id = self.bibframe.keytree.GetPyData(self.bibframe.keytree.GetSelection())[2]
			if id in (BIB.ID_ONLINE_ROOT,BIB.ID_IMPORT_ROOT): return
			#
			self.ref_id = None
			if ref:
				self.ref_id = ref[0]
				for (key_id,) in self.bibframe.db.getKeys(ref[0]):
					self.AppendToPage( """<a href="%s">%s</a><br>\n""" %(cPickle.dumps(self.bibframe.db.getKeyPath(self.bibframe.db.user,key_id)[1]),'_'.join(self.bibframe.db.getKeyPath(self.bibframe.db.user,key_id)[0])) )

	def OnLinkClicked(self,link):
		"""A link has been clicked
		We must go down the keytree of the current db user
		to find the item
		"""
		keypath = cPickle.loads( str(link.GetHref()) )
		#
		self.bibframe.keytree.SelectUserTree()	# we first display the user KeyTree (if we are in the Shared KeyTree)
		parent = self.bibframe.keytree.keyroot
		for key_id in keypath:
			parent = self.__getItem(parent,key_id)
			if not parent: return
			self.bibframe.keytree.Expand(parent)				# we expand the parent key to update the children
		# we now select the found key then the reference
		self.bibframe.keytree.SelectItem(parent)
		item = self.bibframe.reflist.FindItemData(-1, self.ref_id)
		self.bibframe.reflist.Select(item)
		self.bibframe.reflist.EnsureVisible(item)

	def __getItem(self,parentItem,key_id):
		"""Return TreeItem = child of parentItem with Pydata[0] = key_id or False if error"""
		try:
			item,cookie = self.bibframe.keytree.GetFirstChild(parentItem)	# wxPython >= 2.5
			for i in xrange(self.bibframe.keytree.GetChildrenCount(parentItem,False)):
				if self.bibframe.keytree.GetPyData(item)[0] == key_id:
					return item
				item,cookie = self.bibframe.keytree.GetNextChild(parentItem,cookie)
			return False
		except:
			return False

