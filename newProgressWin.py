import wx

class newProgressWin(wx.ProgressDialog):
	def __init__(self,title,message):
		wx.ProgressDialog.__init__(self,_(title),_(message),maximum=100,style = wx.PD_AUTO_HIDE|wx.PD_APP_MODAL|wx.PD_ELAPSED_TIME|wx.PD_ESTIMATED_TIME|wx.PD_CAN_ABORT)

	def Update(self,value,message):
		return wx.ProgressDialog.Update(self,value*100,message)

