# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
import wx
""" program constants
BIB= definition of the references in the db
BIBKEYS= KEYS for tree"""

# Debug
import logging
LEVEL = logging.DEBUG

# constants
BIBUS_VERSION = '1.5.2'
CONFIG_VERSION = None
CONFIG = None	# config file
FIRST_START = True	# first startup of bibus if true => first time help
FILES = ''	# path to the directory containing the full text files (pdf, etc...)
AUTHORTOLONG = 30	# when an author is longer, we issue a warning because there is presumably a missing BIB.SEP (RefEditor)
WILDCARD = "%s (*.pdf)|*.pdf|%s (*.odt)|*.odt|%s (*.doc)|*.doc|%s (*.*)|*.*"\
			%(_("Acrobat file"),\
			_("OpenOffice.org Text Document"),\
			_("Microsoft Word Document"),\
			_("All files"))					# Wildcard used in files open for fulltext links
# Internet access
PUBMED = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils"
PUBMEDVIEW = "http://view.ncbi.nlm.nih.gov/pubmed"
PROXIES = None  # None => system proxy for windows and MacOS.

# keys
KEY_TAGGED = 't'	# keypress to tag a reference (without Ctrl)
KEY_SWITCH_DISPLAY = 'f'	# keypress to switch reference display (bottom-right panel)
# Shortcuts
KEY_QUIT = 'Q'
KEY_NEW = 'N'
KEY_DEL = 'D'
KEY_TAG = 'T'
KEY_SEARCH = 'S'
KEY_PUBMED = 'M'
KEY_COPY = 'C'
KEY_CUT = 'X'
KEY_PASTE = 'V'
KEY_COPYID = 'C'
KEY_SELECT_ALL = 'A'
KEY_PRINT = 'P'
KEY_OO_INSERT = 'I'
KEY_OO_FORMAT = 'F'
KEY_OO_GOWRITER = 'G'
KEY_ETBLAST = 'B'

# Zotero plugin RPC server
ZOTEROPLUGIN = False
#
WP = 'OOo'				# name of the Word Processor in which we insert refs. OOo / mswDoc / Lyx / ''. This is the name of the module to import.
# Openoffice.org constants
bibOOo_cit_baseCharStyleName = 'bibus_cit_base'		# base style for citations
bibOOo_index_baseCharStyleName = 'bibus_index_base'	# base style for bibliography index
OO_CREATE_BIB = True	# True if we must create a bibliographic index if it does not exist
OO_AUTO_UPDATE = True	# True if index update on Insert
OO_HILIGHT = False		# True if citations are hilighted with a yellow background
OO_HILIGHT_COLOR = 0x00FFFF00	# hilight background color
OO_CON_TYPE = 1 		# connection type. 0=TCP/IP ; 1=pipe
OO_HOST = 'localhost'	# host on which OOo is running (TCP/IP)
OO_PORT = 8100			# connection port
OO_PIPE = "OOo_pipe" 	# pipe name for connection
LYX_PIPE = "~/.lyx/lyxpipe"  # Lyx pipe connection (linux only). We will convert to ~/.lyx/lyxpipe.in
BIB_FIELDS=('Id','Identifier', 'BibliographicType', 'Address', 'Annote', 'Author', 'Booktitle', 'Chapter', 'Edition', 'Editor', 'Howpublished', 'Institution', 'Journal', 'Month', 'Note', 'Number', 'Organizations', 'Pages', 'Publisher', 'School', 'Series', 'Title', 'Report_Type', 'Volume', 'Year', 'URL', 'Custom1', 'Custom2', 'Custom3', 'Custom4', 'Custom5', 'ISBN','Abstract')
BIB_TYPE=('ARTICLE','BOOK','BOOKLET','CONFERENCE','INBOOK','INCOLLECTION','INPROCEEDINGS','JOURNAL','MANUAL','MASTERTHESIS','MISC','PHDTHESIS','PROCEEDINGS','TECHREPORT','UNPUBLISHED','EMAIL','WWW','CUSTOM1','CUSTOM2','CUSTOM3','CUSTOM4','CUSTOM5')
BIBLIOGRAPHIC_FIELDS = {}
for i in range(len(BIB_FIELDS)):
	BIBLIOGRAPHIC_FIELDS[BIB_FIELDS[i]] = i
#{'Id':0, 'Identifier':1, 'BibliographicType':2,...'ISBN':31, 'Abstract':32}
BIBLIOGRAPHIC_TYPE = {}
for i in range(len(BIB_TYPE)):
	BIBLIOGRAPHIC_TYPE[BIB_TYPE[i]] = i
# BIBLIOGRAPHIC_TYPE = {'ARTICLE':'0','BOOK':'1','BOOKLET':'2','CONFERENCE':'3','INBOOK':'4','INCOLLECTION':'5','INPROCEEDINGS':'6','JOURNAL':'7','MANUAL':'8','MASTERTHESIS':'9','MISC':'10','PHDTHESIS':'11','PROCEEDINGS':'12','TECHREPORT':'13','UNPUBLISHED':'14','EMAIL':'15','WWW':'16','CUSTOM1':'17','CUSTOM2':'18','CUSTOM3':'19','CUSTOM4':'20','CUSTOM5':'21'}
# images for sort_icon in listRef
# images must be initialized after wx.App: This is done in bibus.py
ARROWS = None
ARROW_UP = None
ARROW_DN = None
# Image list for KeyTree
IMAGESKEY = None
# printing
PRINTER = None		# wx.HtmlEasyPrinting instance
HTML_FONT_SIZE = '',''	# size for HTML text. '','' = normal ; '<small>','</small>' = small ; etc.. ; small or '' is ok for linux ; '' better for Windows
PRINTER_USE_OOo_FORMAT = True	# if we use the OOo format for printing or the format defined by PRINTER_FORMAT
PRINTER_FORMAT = {}	# Define which fields are printed for each BibliographicType. See below for the default values.
PRINTER_COLORS = '000000','000000'	# colors of 'Field Name', 'Field Value' in HTML RVB
PRINTER_STYLE = (1,0,0),(0,0,0)		# (bold,italic,underline),(bold,italic,underline) of name,value
# Databases
DB_TABLE_REF = 'bibref'
DB_TABLE_KEY = 'bibrefKey'
DB_TABLE_LINK = 'bibrefLink'
DB_TABLE_QUERY = 'bibquery'
DB_TABLE_MODIF = 'table_modif'
DB_TABLE_FILE = 'file'
TMP_ONLINE = 'tmp_online'	# temporary table where online search are stored
TMP_IMPORT = 'tmp_import'	# temporary table where imported references are stored
SEP=';'					# list separator in database fields
DB_STARTUP = 0		# 0 = last used db used at startup. 1 = default db
DB_TYPES = ['MySQL','SQLite']
DB_TYPE ='SQLite'	# type of database used 'MySQL','SQlite', etc... => modules dbBibMySQL,dbBibSQLite, etc...
DB_MODULE = None	# contains the module of the currently used connection
# sqlite constants
SQLiteFile = ''
SQLiteUSER = ''
# MySQL constants
DB_NAME = 'Biblio_41'	# Default MySQL database name
SOCKET = '/var/run/mysqld/mysqld.sock'
PORT = 3306
HOST = 'localhost'
USER = ''
PASSWORD = ''
STORE_PASSWD = False	# True if password saved in config file. Unsecure !
# window sizes
TITLE = _("Database = None")	# main window title
WELCOME = _("Welcome to bibus")	# welcome message in wx.StatusBar
WIDTH = 620
HEIGHT = 380
SASH_KEYTREE = 150
SASH_LIST = 200
WX = -1
WY = -1
REF_Ed_X = -1		# refeditor x position
REF_Ed_Y = -1		# y position
REF_Ed_WIDTH = 500	# width. Height auto, depends on reference type
IMPORT_X = -1		# Import text window (ImportFrame.py)
IMPORT_Y = -1
IMPORT_WIDTH = 388
IMPORT_HEIGHT = 392
IMPORT_FORMAT = 0	# Default format for text import (0=="PubMed", 1=="Refer", 2=="RIS")
SEARCH_X = -1		# Search window size
SEARCH_Y = -1
SEARCH_WIDTH = -1
SEARCH_HEIGHT = -1
PUBMED_X = -1		# Search pubmed window size
PUBMED_Y = -1
PUBMED_WIDTH = -1
PUBMED_HEIGHT = -1
PUBMED_NB = 20		# number of pubmed record to import (1..10000)
#
MOVEAUTO = False    # Automatic rename of files and move them in BIB.FILES
#
ENCODING='utf-8'
ENC_ERRORS='strict'
#
LOCALEDIR = ''		# directory for gettext translation files. Value set in bibus.py at startup
SOURCEDIR = ''		# directory where the software is installed. Value set in bibus.py at startup
DOCPATH = ''		# path to the doc html root file. Value set in bibus.py at startup
LICENCEPATH = ''	# path to the Licence txt. Value set in bibus.py at startup
OOPATH = ''			# path OOo/program directory. Value set in bibus.py at startup from bibus.cfg
#
FORMAT_DICO = None	# conversion dictionary for reference formatting. Must be given as a parameter to a Format.Converter object
FORMAT_CONV = None	# conversion dictionary for reference formatting. a Format.Converter object
STYLES = {}		# STYLES[menuId] = filename
STYLE_CURRENT = ''	# filename of the currently used style
STYLE_MENU = None	# the menu to which we must add the styles
#
ROOT=''	# title of the root tree
ALL='All'		# name of the branch containing all the records
QUERY='Query'	# key branch of the queries
IMPORT='Import'
ONLINE='Online'
REF='References'
CITED = 'Cited'
TAGGED = 'Tagged'
TRASH = 'Trash'
CATEGORIES=(ALL,REF,TAGGED,QUERY,ONLINE,IMPORT,CITED,TRASH)	# list of the item to add to root. Names in the db. Order is the order of the keys from top.
Q=_('Queries')
I=_('Import buffer')
A=_('All my references')
O=_('PubMed search')
R=_('References')
C=_('Cited')
T=_('Tagged')
TR= _('Non-classified')
TRANSLATE={QUERY:Q,IMPORT:I,ALL:A,ONLINE:O,REF:R,CITED:C,TAGGED:T, TRASH:TR} # translation db_name -> tree-name
NEWKEY=_('NewKey')	# default name for inserted key
SEP_DUP=u'#'	# character to use in keytree when key duplicated
OPEN_CIT = u'['	# before citation for Drag & Drop
CLOSE_CIT = u']'	# after citation for Drag & Drop
# list display
LIST_DISPLAY = ('Identifier','Year','Author','Title')	# Fields displayed in reflist wx.ListCtrl
LIST_COL_SIZE = (120, 55, 240, 440)	# column sizes if fixed
LIST_COL_SIZE_DEFAULT = 100
LIST_ORDER = 'Identifier'	# Display order in reflist
LIST_HOW = 'ASC'			# How list are sorted by default 'Asc'ending or 'Desc'ending
LIST_SHORT = 50				# We cut the field at this length for display => faster for long author list
LIST_FIELD_FORMAT={}		# this dico allows to format the fields for display/printing
for field in BIB_FIELDS:
	LIST_FIELD_FORMAT[field] = lambda x:x	# do nothing
LIST_FIELD_FORMAT['Id'] = lambda x: str(x)	# we need a string to display
LIST_FIELD_FORMAT['BibliographicType'] = lambda x: NAME_TYPE[BIB_TYPE[int(x)]]	# convert number to BibliographicType name for display
#
DISPLAY_FORMAT = 0		# Format in the bottom-right panel. Tab number. 0=OOo ; 1=Full ; 2=Keys ; ...
KEY_COLOR = wx.RED		# color display of keys (Title, AUthor, etc..) in textCtrl-1
TEXT_COLOR = wx.BLACK	# color display of text (Title, Author, etc..) in textCtrl-1
DnD_FORMAT = wx.CustomDataFormat('bibus/dnd') # the custom format for DnD
DnD_REF = 'bibus/ref'	# the custom format for DnD of references
DnD_KEY = 'bibus/key'	# the custom format for DnD of keys
TEXT_CLIPBOARD = 'Refer'	# format of the text copied in clipboard. Must be an export filter name in Export directory EXCEPT 'html' which is already copied.
BIB_SEARCH_FIELDS=('Annote', 'Author', 'Booktitle', 'Journal', 'Note', 'Title', 'Year', 'Custom1', 'Custom2', 'Custom3', 'Custom4', 'Custom5', 'Abstract')
#
SHORTCUTS = [_("Author"),_("Journal"),_("Divers")]	# list of Shortcut categories
# SHORTFILE = []	# list of path to the files containing the pickle list of shortcuts
SHORT = {}		# dir of list of shorcuts for each category SHORT['Author']=['Martin','John',...]
SHORTCUT_DEFAULT = _('shortcut')	# default value for shortcuts
# constant to define allowed actions in menu. Use bit comparison
RENAME_OK = 2**0	# if possible to edit the name
DELETE_OK = 2**10	# if possible to delete the key
CHILD_OK = 2**1		# if menu add child ok
DRAG_OK = 2**2		# if the key can be dragged
DROP_OK = 2**3		# if a key can be dropped on it
SELECT_OK = 2**4	# if the key can be selected
REF_EDIT_OK = 2**5	# if a ref associated with this key can be edited
REF_DEL_OK = 2**6	# if a ref associated with this key can be deleted
REF_NEW_OK = 2**7	# if a ref associated with this key can be created
REF_DRAG_OK = 2**8	# if a ref associated with this key can be dragged to another key
REF_DROP_OK =2**9	# if a ref can be dropped on this key
OOo_INSERT_OK = 2**11	#  if a ref associated with this key can be inserted in OOo
EXPORT_OK = 2**12	# is it possible to export references associated with this key ?
REF_TAG_OK = 2**13	# if a ref associated with this key can be tagged
REF_PRINT_OK = 2**14	# if we can print the reference
REF_DUP_OK = 2**15	# if a ref associated with this key can be duplicated

ALL_OK = -1
READ_ONLY = REF_DRAG_OK | SELECT_OK
# constant to identify key in the tree
ID_ROOT = 2**0
ID_REF_ROOT = 2**1
ID_QUERY_ROOT = 2**2
ID_ONLINE_ROOT = 2**3
ID_IMPORT_ROOT = 2**4
ID_REF = 2**5
ID_REF_ALL = 2**6	# item ALL refs of key REF
ID_QUERY = 2**7
ID_ONLINE = 2**8
ID_IMPORT = 2**9
ID_CITED = 2**10	# item 'Cited in current OOo doc'
ID_TAGGED = 2**11
ID_TRASH = 2**12
# The following directory: give the children type. Used when a children is created.
ID_CHILDREN = {ID_REF_ROOT:ID_REF,ID_QUERY_ROOT:ID_QUERY,ID_ONLINE_ROOT:ID_ONLINE,ID_IMPORT_ROOT:ID_IMPORT,
ID_REF:ID_REF,ID_QUERY:ID_QUERY,ID_ONLINE:ID_ONLINE,ID_IMPORT:ID_IMPORT}

# allowed actions for keys
ALLOWED={}
# default values = user is read+write
ALLOWED['rw']={}
ALLOWED['rw'][ID_ROOT]= 0
ALLOWED['rw'][ID_REF_ROOT]= CHILD_OK | DROP_OK | SELECT_OK | REF_EDIT_OK | REF_DEL_OK | REF_NEW_OK | REF_DRAG_OK | REF_DROP_OK | OOo_INSERT_OK | EXPORT_OK | REF_TAG_OK | REF_PRINT_OK | REF_DUP_OK
ALLOWED['rw'][ID_QUERY_ROOT] = READ_ONLY 	#| EXPORT_OK | REF_PRINT_OK | REF_DUP_OK
ALLOWED['rw'][ID_ONLINE_ROOT] = READ_ONLY
ALLOWED['rw'][ID_IMPORT_ROOT] = READ_ONLY
ALLOWED['rw'][ID_REF] = ALL_OK
ALLOWED['rw'][ID_QUERY] = REF_DRAG_OK | SELECT_OK | REF_EDIT_OK | OOo_INSERT_OK | EXPORT_OK | REF_TAG_OK | REF_PRINT_OK | REF_DUP_OK
ALLOWED['rw'][ID_ONLINE] = READ_ONLY
ALLOWED['rw'][ID_IMPORT] = READ_ONLY
ALLOWED['rw'][ID_REF_ALL] = SELECT_OK | REF_EDIT_OK | REF_NEW_OK | REF_DRAG_OK | OOo_INSERT_OK | EXPORT_OK | REF_TAG_OK | REF_PRINT_OK | REF_DUP_OK
ALLOWED['rw'][ID_CITED] = REF_DRAG_OK | SELECT_OK | REF_EDIT_OK | OOo_INSERT_OK | EXPORT_OK | REF_TAG_OK | REF_PRINT_OK | REF_DUP_OK
ALLOWED['rw'][ID_TAGGED] = SELECT_OK | REF_EDIT_OK | REF_DEL_OK | REF_NEW_OK | REF_DRAG_OK | REF_DROP_OK | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK | REF_DUP_OK
ALLOWED['rw'][ID_TRASH] = READ_ONLY
# values for a read-only user with permissions to classify his references
ALLOWED['ro']={}
ALLOWED['ro'][ID_ROOT]= 0
ALLOWED['ro'][ID_REF_ROOT]= CHILD_OK | DROP_OK | SELECT_OK | REF_DEL_OK | REF_DRAG_OK | REF_DROP_OK | OOo_INSERT_OK | EXPORT_OK | REF_TAG_OK | REF_PRINT_OK
ALLOWED['ro'][ID_QUERY_ROOT] = READ_ONLY 	#| EXPORT_OK | REF_PRINT_OK
ALLOWED['ro'][ID_ONLINE_ROOT] = READ_ONLY
ALLOWED['ro'][ID_IMPORT_ROOT] = READ_ONLY
ALLOWED['ro'][ID_REF] = RENAME_OK| DELETE_OK | CHILD_OK | DROP_OK | SELECT_OK | REF_DEL_OK | REF_DRAG_OK | REF_DROP_OK | OOo_INSERT_OK | EXPORT_OK | REF_TAG_OK | REF_PRINT_OK
ALLOWED['ro'][ID_QUERY] = REF_DRAG_OK | SELECT_OK | OOo_INSERT_OK | EXPORT_OK | REF_TAG_OK | REF_PRINT_OK
ALLOWED['ro'][ID_ONLINE] = READ_ONLY
ALLOWED['ro'][ID_IMPORT] = READ_ONLY
ALLOWED['ro'][ID_REF_ALL] = SELECT_OK | REF_DRAG_OK | OOo_INSERT_OK | EXPORT_OK | REF_TAG_OK | REF_PRINT_OK
ALLOWED['ro'][ID_CITED] = REF_DRAG_OK | SELECT_OK | OOo_INSERT_OK | EXPORT_OK | REF_TAG_OK | REF_PRINT_OK
ALLOWED['ro'][ID_TAGGED] = SELECT_OK | REF_DEL_OK | REF_DRAG_OK | REF_DROP_OK | EXPORT_OK | OOo_INSERT_OK | REF_PRINT_OK
ALLOWED['ro'][ID_TRASH] = READ_ONLY
# values for a read-only user able to read also the bibrefKey table. Can see the classification
ALLOWED['rk']={}
ALLOWED['rk'][ID_ROOT]= 0
ALLOWED['rk'][ID_REF_ROOT]= READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rk'][ID_QUERY_ROOT] = READ_ONLY 	#| EXPORT_OK | REF_PRINT_OK
ALLOWED['rk'][ID_ONLINE_ROOT] = READ_ONLY
ALLOWED['rk'][ID_IMPORT_ROOT] = READ_ONLY
ALLOWED['rk'][ID_REF] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rk'][ID_QUERY] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rk'][ID_ONLINE] = READ_ONLY
ALLOWED['rk'][ID_IMPORT] = READ_ONLY
ALLOWED['rk'][ID_REF_ALL] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rk'][ID_CITED] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rk'][ID_TAGGED] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rk'][ID_TRASH] = READ_ONLY
# values for a read-only user with restricted permissions. Read-only for all tables.
ALLOWED['rr']={}
ALLOWED['rr'][ID_ROOT]= 0
ALLOWED['rr'][ID_REF_ROOT]= READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rr'][ID_QUERY_ROOT] = READ_ONLY 	#| EXPORT_OK | REF_PRINT_OK
ALLOWED['rr'][ID_ONLINE_ROOT] = READ_ONLY
ALLOWED['rr'][ID_IMPORT_ROOT] = READ_ONLY
ALLOWED['rr'][ID_REF] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rr'][ID_QUERY] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rr'][ID_ONLINE] = READ_ONLY
ALLOWED['rr'][ID_IMPORT] = READ_ONLY
ALLOWED['rr'][ID_REF_ALL] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rr'][ID_CITED] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rr'][ID_TAGGED] = READ_ONLY | OOo_INSERT_OK | EXPORT_OK | REF_PRINT_OK
ALLOWED['rr'][ID_TRASH] = READ_ONLY

# Shared keytree
# list of possible editor for 'Shared' category
# ('Admin',) only user='Admin' can edit it
# () nobody can edit it, except 'Admin' when logged as 'Admin'
# ('*',) all users can edit it
# ('Admin','toto') 'Admin' and 'toto' can edit it
SHARED_DBNAME = "Admin"		# Administrator name in the database
SHARED_NAME = _("Shared")	# Name of the shared panel
ADMIN = ('*',) # list of users that can edit this shared category. ('*',) = All users with write access, ie rw users; don't forget the comma if the list contains a single object
MULTI_USER = True			# if we are in a multi_users environment => mutiple keytree

# Database fields definition
# TABLE_REF='(\
# %s int UNSIGNED NOT NULL AUTO_INCREMENT primary key,\
# %s varchar(255) NOT NULL,\
# %s tinyint UNSIGNED NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s TEXT NOT NULL)' % BIB_FIELDS

# This table maps ref to keys. A ref can be mapped to several keys.
# user = user name. To know to which user the link belongs to.
# ref_Id = unique Id of bibref table
# user=user name
# key_id= unique Id of bibrefKey table
# the pair must be unique
BIB_LINKS=('key_Id','ref_Id')
# TABLE_LINK='(\
# %s int UNSIGNED NOT NULL,\
# %s int UNSIGNED NOT NULL,\
# UNIQUE link(%s,%s))' % (BIB_LINKS + BIB_LINKS)

# This table describes the tree of keys
# user = user name. To know to which user the key belongs to.
# key_id = unique Id of bibrefKey table
# parent = key_id of parent key. NULL = root
# key_name = key name
BIB_KEYS=('user','key_Id','parent','key_name')
# TABLE_KEY='(\
# %s varchar(255) NOT NULL,\
# %s int UNSIGNED NOT NULL AUTO_INCREMENT primary key,\
# %s int UNSIGNED,\
# %s varchar(255) NOT NULL)' % BIB_KEYS

# This table if for queries storage
BIB_QUERY=('query_id','user','name','query')
# TABLE_QUERY='(\
# %s int UNSIGNED NOT NULL AUTO_INCREMENT primary key,\
# %s varchar(255) NOT NULL,\
# %s varchar(255) NOT NULL,\
# %s TEXT NOT NULL)' % BIB_QUERY
BIB_MODIF=('ref_Id','creator','date','user_modif','date_modif')
BIB_FILE = ('ref_Id','path')

# default value for : Fields displayed in the textCtrl when a ref is selected
BIB_PRINT_FIELD=('Author','Title','Year','Journal','Volume','Pages','Abstract')
# In fact, we display the required fields of the EDIT dictionary + Abstract = EDIT["ARTICLE"][0]+("Abstract",)

# Fields for Reference Editing ((main fields,...),(supp fields,...)) (other fields,...) obtained by substraction
EDIT={}
EDIT["ARTICLE"]=(("Author","Title","Journal","Volume","Year","Pages"),("Number","Month","Note","Institution","Address","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["BOOK"]=(("Author","Title","Editor","Publisher","Year"),("Volume","Number","Series","Edition","Month","Note","Address","URL","ISBN","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["BOOKLET"]=(("Title",),("Author","Year","Month","Note","Address","Howpublished","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["CONFERENCE"]=(("Author","Title","Booktitle","Year"),("Editor","Volume","Number","Series","Pages","Address","Month","Organizations","Publisher","Note","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["INBOOK"]=(("Author","Title","Chapter","Year","Pages","Booktitle","Editor","Publisher"),("Volume","Number","Month","Address","Note","Edition","Series","Report_Type","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["INCOLLECTION"]=(("Author","Title","Journal","Volume","Year","Pages"),("URL","Month","Number","Institution","Address","Annote","Note","Booktitle","Chapter","Edition","Editor","Howpublished","Organizations"))
EDIT["INPROCEEDINGS"]=EDIT["CONFERENCE"]
EDIT["JOURNAL"]=(("Journal",),("Author","Title","Volume","Month","Number","Year","Pages","Note","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["MANUAL"]=(("Title",),("Author","Year","Month","Address","Edition","Organizations","Note","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["MASTERTHESIS"]=(("Author","Title","School","Year"),("Address","Note","Month","URL","Report_Type","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["MISC"]=(("Author","Title","Howpublished","Month","Year","Note"),("Pages","Journal","Volume","Number","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["PHDTHESIS"]=(("Author","Title","School","Year"),("Pages","Note","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["PROCEEDINGS"]=(("Title","Year"),("Volume","Month","Number","Address","Editor","Publisher","Series","Organizations","Note","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["TECHREPORT"]=(("Author","Title","Institution","Year"),("Month","Number","Address","Report_Type","Note","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["UNPUBLISHED"]=(("Author","Title","Note"),("Month","Year","URL","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["EMAIL"]=(("URL",),("Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["WWW"]=(("URL",),("Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["CUSTOM1"]=(("Author","Title","Year","Howpublished"),("Journal","Volume","Pages","URL","Month","Number","Institution","Address","Annote","Note","Custom1","Custom2","Custom3","Custom4","Custom5"))
EDIT["CUSTOM5"]=EDIT["CUSTOM4"]=EDIT["CUSTOM3"]=EDIT["CUSTOM2"]=EDIT["CUSTOM1"]
# Refeditor style of textCtrl
EDIT_STYLE={}
for field in BIB_FIELDS: EDIT_STYLE[field] = 0
EDIT_STYLE["Identifier"] = 0 # wx.TE_READONLY
EDIT_STYLE["Author"] = wx.TE_MULTILINE
EDIT_STYLE["Title"] = EDIT_STYLE["Author"]
EDIT_STYLE['Booktitle'] = EDIT_STYLE["Author"]
EDIT_STYLE['Editor'] = EDIT_STYLE["Author"]
EDIT_STYLE['Abstract'] = EDIT_STYLE["Author"]
EDIT_STYLE['Note'] = EDIT_STYLE["Author"]
EDIT_STYLE['Address'] = EDIT_STYLE["Author"]
# Renaming of fields. 
# we use it to translate field and eventually to rename them. For instance it is usefull to rename Custom1 etc...
# for instance: NAME_FIELD["Custom1"] = "Keyword" ... The right place to do this is in bibus.config
NAME_FIELD = {'Id':_('Id'),'Identifier':_('Identifier'),'BibliographicType':_('BibliographicType'),'Address':_('Address'),'Annote':_('Annote'),'Author':_('Author'),'Booktitle':_('Booktitle'),'Chapter':_('Chapter'),'Edition':_('Edition'),'Editor':_('Editor'),'Howpublished':_('Howpublished'),'Institution':_('Institution'),'Journal':_('Journal'),'Month':_('Month'),'Note':_('Note'),'Number':_('Number'),'Organizations':_('Organizations'),'Pages':_('Pages'),'Publisher':_('Publisher'),'School':_('School'),'Series':_('Series'),'Title':_('Title'),'Report_Type':_('Report_Type'),'Volume':_('Volume'),'Year':_('Year'),'URL':_('URL'),'Custom1':_('Custom1'),'Custom2':_('Custom2'),'Custom3':_('Custom3'),'Custom4':_('Custom4'),'Custom5':_('Custom5'),'ISBN':_('ISBN'),'Abstract':_('Abstract')}
# we reverse the dictionary
FIELD_NAME={} # this will be done in bibus.py in case NAME_FIELD is modified by the user in bibus.config
#
# Renaming of types
# we use it to translate field and eventually to rename them. For instance it is usefull to rename CUSTOM1 etc...
# for instance: NAME_FIELD["CUSTOM1"] = "PATENT" ...
NAME_TYPE = {'ARTICLE':_('ARTICLE'),'BOOK':_('BOOK'),'BOOKLET':_('BOOKLET'),'CONFERENCE':_('CONFERENCE'),'INBOOK':_('INBOOK'),'INCOLLECTION':_('INCOLLECTION'),'INPROCEEDINGS':_('INPROCEEDINGS'),'JOURNAL':_('JOURNAL'),'MANUAL':_('MANUAL'),'MASTERTHESIS':_('MASTERTHESIS'),'MISC':_('MISC'),'PHDTHESIS':_('PHDTHESIS'),'PROCEEDINGS':_('PROCEEDINGS'),'TECHREPORT':_('TECHREPORT'),'UNPUBLISHED':_('UNPUBLISHED'),'EMAIL':_('EMAIL'),'WWW':_('WWW'),'CUSTOM1':_('CUSTOM1'),'CUSTOM2':_('CUSTOM2'),'CUSTOM3':_('CUSTOM3'),'CUSTOM4':_('CUSTOM4'),'CUSTOM5':_('CUSTOM5')}
TYPE_NAME = {} # see note above about FIELD_NAME
# Printing default
for articleType in BIB_TYPE:
	PRINTER_FORMAT[articleType] = ('Identifier','BibliographicType') + EDIT[articleType][0] + ("Abstract",)

# Duplicates. tuple of Fields tested to decide it 2 ref are identical or not.
DUPLICATES_TEST = True	# True if we want to test for duplicate entries
DUPLICATES = ('BibliographicType', 'Author', 'Booktitle', 'Chapter', 'Edition', 'Journal', 'Pages', 'Series', 'Title', 'Volume', 'Year')
DUPLICATES_CASE = False	# True == comparison is Case sensitive
DUPLICATES_KEEP_OLD = True	# True = in case of duplicate we keep the old ref, False = we keep the new

# Journal abbreviations
# JOURNAL[MedAbbr.upper()] = JOURNAL[MedAbbr.upper()] = JOURNAL[MedAbbr.upper()] = [MedAbbr,IsoAbbr,JournalTitle]
# JOURNAL_ALTERNATE[MedAbbr.upper()] = JOURNAL_ALTERNATE[MedAbbr.upper()] = JOURNAL_ALTERNATE[MedAbbr.upper()] = MedAbbr.upper()
JOURNAL_FILE = 'journals.csv'
JOURNAL = {}			# keys == PubMed
JOURNAL_ALTERNATE = {}	# keys == PubMed => values PubMed, ISO, Full => JOURNAL[ JOUNAL_ALTERNATE['journal'] ] = PubMed,Iso,Full
JOURNAL_MODIF = False

# The following function return a Pattern used for automatic search of fulltext files in $FILES
PATTERN = []
PATTERN.append( lambda ref: ".*%(Author)s.*%(Year)s.*" %{'Author': ref[BIBLIOGRAPHIC_FIELDS['Author']].split(SEP)[0].split(',')[0],'Year': ref[BIBLIOGRAPHIC_FIELDS['Year']]} )
PATTERN.append( lambda ref: ".*%(Identifier)s.*" %{'Identifier': ref[BIBLIOGRAPHIC_FIELDS['Identifier']] } )

