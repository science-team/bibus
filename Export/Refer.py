# Copyright 2004,2005 Pierre Martineau <pmartino@users.sourceforge.net>
# This file is part of Bibus, a bibliographic database that can
# work together with OpenOffice.org to generate bibliographic indexes.
#
# Bibus is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bibus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Bibus; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA.
#
# EndNote refer format
# from http://www.ecst.csuchico.edu/~jacobsd/bib/formats/endnote.html
#
import BIB

DEFAULT_ENCODING = 'cp1252'

class exportRef(object):
	"""Class is iterable. Return records one by one."""
	# conversion OOo <-> EndNote Publication Type: dictionary Type[OOo Name]=EndNote/refed.
 	Type={
	'ARTICLE':'Journal Article' ,
	'BOOK':'Book' ,
	'BOOKLET':'Book' ,
	'CONFERENCE':'Conference Proceedings' ,
	'INBOOK':'Book Section' ,
	'INCOLLECTION':'Book' ,
	'INPROCEEDINGS':'Conference Proceedings' ,
	'JOURNAL':'Journal Article' ,
	'MANUAL':'Report' ,
	'MASTERTHESIS':'Thesis' ,
	'MISC':'Generic' ,
	'PHDTHESIS':'Thesis' ,
	'PROCEEDINGS':'Conference Proceedings' ,
	'TECHREPORT':'Report' ,
	'UNPUBLISHED':'Personal Communication' ,
	'EMAIL':'Generic' ,
	'WWW':'Generic' ,
	'CUSTOM1':'Generic' ,
	'CUSTOM2':'Generic' ,
	'CUSTOM3':'Generic' ,
	'CUSTOM4':'Generic' ,
	'CUSTOM5':'Generic'}

	def __init__(self,infile):
		self.infile = infile	# must be a file type. Need a write() function.

	def write(self,ref):
		"""write(ref)"""
		record = self.__convertRecord(ref)
		if record:
			self.infile.write(record)
			self.infile.write('\n\n')		# add a blank line to separate records

	def __convertRecord(self,ref):
		"""Convert a OOo reference to an endnote record
		input ==
		('Id','Identifier', 'Bibliographic_Type', 'Address', 'Annote', 'Author', 'Booktitle', 'Chapter', 'Edition', 'Editor','HowPublished', 'Institution', 'Journal', 'Month', 'Note', 'Number', 'Organizations', 'Pages', 'Publisher', 'School', 'Series', 'Title', 'Report_Type', 'Volume', 'Year', 'URL', 'Custom1', 'Custom2', 'Custom3', 'Custom4', 'Custom5', 'ISBN','Abstract')"""
		#
		record=[]
		# Type
		record.append( "%%0 %s"%exportRef.Type[BIB.BIB_TYPE[ref[2]]] )
		# Address
		if ref[3]: record.append( "%%C %s"%ref[3] )
		# Annote
		if ref[4]: record.append( "%%U %s"%ref[4] )
		# Author
		if ref[5]:
			record.extend( map( lambda x: "%%A %s"%x , ref[5].split(BIB.SEP) ) )
		# Booktitle
		if ref[6]:
			if BIB.BIB_TYPE[ref[2]] == 'INBOOK':
				record.append( "%%B %s"%ref[6] )
			else:
				record.append( "%%T %s"%ref[6] )
		# Chapter
		# if ref[7]: Don't know where to put the chapter
		# Edition
		if ref[8]:
			record.append( "%%7 %s"%ref[8] )
		# Editor
		if ref[9]:
			record.extend( map( lambda x: "%%E %s"%x , ref[9].split(BIB.SEP) ) )
		# HowPublished
		if ref[10]:
			record.append( "%%9 %s"%ref[10] )
		# Institution
		if ref[11]:
			record.append( "%%Q %s"%ref[11] )
		elif ref[16]:
			record.append( "%%Q %s"%ref[16] )
		# Journal
		if ref[12]:
			record.append( "%%J %s"%ref[12] )
		# Year(24) Month(13)
		if ref[24]:
			record.append( "%%D %s %s"%(ref[24],ref[13]) )
		# Note
		#if ref[14]:
		# Number
		if ref[15]:
			record.append( "%%N %s"%ref[15] )
		# Organizations
		# see Institution
		# Pages
		if ref[17]:
			record.append( "%%P %s"%ref[17] )
		# Publisher
		if ref[18]:
			record.append( "%%I %s"%ref[18] )
		# School
		# if ref[19]:
		# Series
		if ref[20]:
			record.append( "%%S %s"%ref[20] )
		# Title
		if ref[21]:
			record.append( "%%T %s"%ref[21] )
		# Report_Type
		if ref[22]:
			record.append( "%%9 %s"%ref[22] )
		# Volume
		if ref[23]:
			record.append( "%%V %s"%ref[23] )
		# Year
		# see Month
		# URL
		if ref[25]:
			record.append( "%%W %s"%ref[25] )
		# Custom1 ... Custom5
		# Where to put them ?
		# ISBN
		#if ref[31]:
		# Abstract
		if ref[32]:
			record.append( "%%X %s"%ref[32] )
		#
		return '\n'.join(record)
