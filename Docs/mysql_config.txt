﻿Creation date: 2004.05.18
Last revision: 2004.07.29

*************************************************
** Installing the mysql bibliographic database **
*************************************************
1) untar/unzip bibus:
	prompt> tar -zxf bibus-0.8.tgz
2) cd bibus-0.8/Docs/db_models
You will find in this directory two databases:
	- mysql.sql
3) If you want to use MySQL. Use your prefered way to talk to MySQL, either using a graphical interface
   like MySQLcc or the terminal application mysql, or any other means.
	a) create a database in MySQL, named for instance 'Biblio'.
		mysql> create database Biblio;
	b) you must have the following GRANTS to use the database :
		select,update,insert,delete,create temporary table
	c) create the database tables using the database suplied in Bibus/Docs/db_models (mysql.sql) and
           for instance mysql program:
		mysql -p Biblio < mysql.sql
	depending on your GRANTS you may need to run this command as root (the root of MySQL)
		mysql -u root -p Biblio < mysql.sql
	d) The database is now ready.


