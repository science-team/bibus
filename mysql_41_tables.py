
TABLE_QUERY = """
(
  query_id int(10) unsigned NOT NULL auto_increment,
  user varchar(255) NOT NULL default '',
  name varchar(255) NOT NULL default 'query',
  query text NOT NULL,
  PRIMARY KEY  (query_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
"""

TABLE_REF = """
(
  Id int(10) unsigned NOT NULL auto_increment,
  Identifier varchar(255) default NULL,
  BibliographicType tinyint(3) unsigned NOT NULL default '0',
  Address varchar(255) NOT NULL default '',
  Annote varchar(255) NOT NULL default '',
  Author text NOT NULL,
  Booktitle varchar(255) NOT NULL default '',
  Chapter varchar(255) NOT NULL default '',
  Edition varchar(255) NOT NULL default '',
  Editor varchar(255) NOT NULL default '',
  Howpublished varchar(255) NOT NULL default '',
  Institution varchar(255) NOT NULL default '',
  Journal varchar(255) NOT NULL default '',
  Month varchar(255) NOT NULL default '',
  Note varchar(255) NOT NULL default '',
  Number varchar(255) NOT NULL default '',
  Organizations varchar(255) NOT NULL default '',
  Pages varchar(255) NOT NULL default '',
  Publisher varchar(255) NOT NULL default '',
  School varchar(255) NOT NULL default '',
  Series varchar(255) NOT NULL default '',
  Title varchar(255) NOT NULL default '',
  Report_Type varchar(255) NOT NULL default '',
  Volume varchar(255) NOT NULL default '',
  Year varchar(255) NOT NULL default '',
  URL varchar(255) NOT NULL default '',
  Custom1 varchar(255) NOT NULL default '',
  Custom2 varchar(255) NOT NULL default '',
  Custom3 varchar(255) NOT NULL default '',
  Custom4 varchar(255) NOT NULL default '',
  Custom5 varchar(255) NOT NULL default '',
  ISBN varchar(255) NOT NULL default '',
  Abstract text NOT NULL,
  PRIMARY KEY  (Id),
  UNIQUE KEY Identifier (Identifier)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
"""

TMPTABLE_REF = """
(
  Id int(10) unsigned NOT NULL auto_increment,
  Identifier varchar(255) default NULL,
  BibliographicType tinyint(3) unsigned NOT NULL default '0',
  Address varchar(255) NOT NULL default '',
  Annote varchar(255) NOT NULL default '',
  Author text NOT NULL,
  Booktitle varchar(255) NOT NULL default '',
  Chapter varchar(255) NOT NULL default '',
  Edition varchar(255) NOT NULL default '',
  Editor varchar(255) NOT NULL default '',
  Howpublished varchar(255) NOT NULL default '',
  Institution varchar(255) NOT NULL default '',
  Journal varchar(255) NOT NULL default '',
  Month varchar(255) NOT NULL default '',
  Note varchar(255) NOT NULL default '',
  Number varchar(255) NOT NULL default '',
  Organizations varchar(255) NOT NULL default '',
  Pages varchar(255) NOT NULL default '',
  Publisher varchar(255) NOT NULL default '',
  School varchar(255) NOT NULL default '',
  Series varchar(255) NOT NULL default '',
  Title varchar(255) NOT NULL default '',
  Report_Type varchar(255) NOT NULL default '',
  Volume varchar(255) NOT NULL default '',
  Year varchar(255) NOT NULL default '',
  URL varchar(255) NOT NULL default '',
  Custom1 varchar(255) NOT NULL default '',
  Custom2 varchar(255) NOT NULL default '',
  Custom3 varchar(255) NOT NULL default '',
  Custom4 varchar(255) NOT NULL default '',
  Custom5 varchar(255) NOT NULL default '',
  ISBN varchar(255) NOT NULL default '',
  Abstract text NOT NULL,
  PRIMARY KEY  (Id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
"""

TABLE_KEY = """
(
  user varchar(255) NOT NULL default '',
  key_Id int(10) unsigned NOT NULL auto_increment,
  parent int(10) unsigned default NULL,
  key_name varchar(255) NOT NULL default 'newkey',
  PRIMARY KEY  (key_Id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
"""

TABLE_LINK = """
(
  key_Id int(10) unsigned NOT NULL default '0',
  ref_Id int(10) unsigned NOT NULL default '0',
  UNIQUE KEY link (key_Id,ref_Id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
"""

TABLE_MODIF = """
(
  ref_Id int(10) unsigned NOT NULL default '0',
  creator varchar(255) NOT NULL default '',
  date double NOT NULL default 0,
  user_modif varchar(255) NOT NULL default '',
  date_modif double NOT NULL default 0,
  UNIQUE ref_Id (ref_Id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
"""

TABLE_FILE = """
(
  ref_Id int(10) unsigned NOT NULL default '0',
  path varchar(255) NOT NULL default ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8
"""

